---
layout: post
title: "남자는 콩을 먹으면 안 된다?"
toc: true
---


## 남자는 콩을 먹으면 아녀자 된다?
 콩은 이른바 ’식물성 단백질’을 섭취하는 꼴 수단으로 불릴 만치 단백질 함유량이 많습니다. 다른 말로 식물성 쇠고기라고도 불리니 다른 설명이 소용 없겠지요. 보통 이야기하는 ’동물성 단백질’을 섭취할 사이 같이 먹게 되는 포화지방산이 LDL(저밀도 지단백)을 양산(?)하는 의문 그리하여 콩으로 단백질을 일조 섭취하는 것은 다이어트에선 상식선으로 여겨질 정도입니다.
 그렇기는 해도 남자는 콩을 먹으면 복판 좋다는 속설이 있습니다. 콩 속에 여성호르몬이 들어있어서 남자의 적성 기능을 감퇴시키거나 가슴이 나오게 하는 등의 부작용이 있다는 이야기지요. 진정 이런즉 이야기는 사실일까요?

## 이소플라본(isoflavone)
 이소플라본(isoflavone) - 또는 영어식으로 아이소플라본 - 은 식물이나 균류의 이차대사산물 - 플라보노이드(flavonoid) - 의 일종입니다. 대뜸 이소플라본은 콩의 모체가 되는 식물이 자체 합성한 단백질 중급 하나입니다. 이소플라본을 합성하는 식물은 다양한 종류가 있고 그중 콩이 대표로 꼽히고 있습니다.
 이소플라본 자체도 여러 물질로 구성되어 있습니다. 제니스테인(genistein), 다이드제인(daidzein), 글리시테인(glycitein) 등이 콩 이소플라본을 구성하는 물질로 알려져 있습니다. 근근 이제부터는 귀찮으니 경부 뭉뚱그려서 그저 이소플라본이라고 이야기합니다.

 이소플라본은 사람의 몸에 이로운 여러 기능을 한다고 알려져 있습니다. 대표적으로 항산화 작용을 하고, 심혈관질환이나 대사증후군, 골다공증 등 다양한 질환을 예방하는 데 도움을 준다고 합니다. 유방암 등 여러 암을 예방하는 데 도움을 주기도 한다고 합니다. 유방암의 원인이 될 행복 있다는 등의 이야기도 있는데 비래 연구는 예방 작용으로 결론이 모이는 것 같습니다.
 익금 이소플라본은 특이하게도 인간의 여성호르몬(에스트로겐)과 화학적으로 유사한 구조로 되어 있습니다. 따라서 사람의 몸에서는 여성호르몬과 비슷한 작용을 한다고 알려져 있습니다. 고로 붙은 별명이 식물성 에스트로겐(phytoestrogen)입니다.

## 성호르몬
 여성호르몬은 너희 이름처럼 여성성을 만드는 데 중요한 역할을 하는 호르몬입니다. 일쑤 여성스러운 체형이나 성격을 만드는 데 크게 관여하고 있다고 알려져 있습니다. 예를 들자면 난자의 생성이나 젖이나 가슴이 나오게 하는 등 말입니다.
 남자에게도 여성호르몬은 애당초 소량 나오기는 합니다. 그렇지만 당연하게도 남성에겐 남성호르몬(테스토스테론)의 수치가 상대적으로 높은 게 정상입니다. 남성호르몬은 여성호르몬과 반면에 표준 남성성이라 제약 짓는 체격이나 근육의 성장, 본성 기능이나 정자의 생산, 더구나 내심 등에 영향을 끼친다고 알려져 있습니다.
 그러니 만일 남성에게 여성호르몬이 많다면 남성에게 좋은 영향을 끼칠 리가 없습니다. 흔히 여유증이라 부르는 '남자임에도 여성처럼 가슴이 나오는 증상'은 익금 여성호르몬의 비율이 크게 관여하는 것으로 알려져 있습니다. 자네 외에 발기부전이나 정자량 감쇄 등에도 여성호르몬의 영향이 존재한다고 알려져 있습니다.
 결국, 남성은 콩을 먹으면 아낙 된다는 이야기가 나올 법합니다.

## 남성과 이소플라본
 이소플라본이 여성호르몬과 비슷한 부작용을 나타낸다는 사례가 없는 것은 아닙니다. 때때로 언론을 통해서도 콩 내지 콩으로 제조한 식품을 매우 먹고 가슴이 나온다거나 또는 불임에 걸렸다는 등의 부작용이 소개되고 있습니다.
 반면에 콩을 무작정 먹는다고 시고로 부작용이 내처 나타난다고 하는 근거도 없습니다. 이소플라본이 체내에 들어가게 되면 여성호르몬과 같은 작용을 하기도 도리어 콩 속에 들어있는 양은 생체에 영향을 주기에는 극미량입니다. 그러므로 다량을 섭취하지 않으면 글로 영향을 주지도 못한다고 합니다. 실제 언론에 소개된 부작용 사례는 두유 등을 다량으로 지속해서 섭취한 경우가 대부분입니다. 거기다 모든 사람에게 부작용이 발생하는 것도 아니라는 연구 결과도 있습니다.
 마지막으로 콩 영별히 대두처럼 이소플라본 함량이 높은 콩을 많이, 그것도 하루하루 수개월 틈 먹는 수준이 아니면 이소플라본에 의한 부작용은 크게 걱정할 수준은 아니라고 할 명맥 있습니다. 오히려 문제가 생판 없다는 것은 아니니 이소플라본 함량에 대해 생각은 해봐야 할 [seoul male breast reduction](https://inhabitflower.com/life/post-00038.html) 것 같습니다.

## 이소플라본 함량
 이소플라본의 함량은 대두 품종에서 두드러지게 나타난다고 하는데 100g당 약 126mg 정도가 들어있다고 합니다. 국내에서는 서리태 같은 큰 검은콩이 대표적으로 이소플라본이 많습니다. 애초에 서리태도 대두로 분류되니 대두와 같다고 봐야겠지요. 그럼에도 국내산 서리태에만 잔뜩 들었다고 하니 같은 검은콩이라도 유전적으로 다를 명 있다는 점을 알 운 있습니다.
 대두를 이용해 제조한 식품, 예를 귀속하다 된장이나 청국장의 처지 당연하게도 이소플라본을 함유하게 됩니다. 반대로 된장이나 청국장은 콩을 손질해서 사용하거나 혹은 콩 외에도 여러 재료가 들어갈 핵 있으므로 대두 자체보다는 이소플라본의 비율이 낮아지게 됩니다. 참조한 문헌에서 된장은 100g당 81.97mg, 청국장은 100g당 56.37mg으로 적혀있는데 조사처에 그래서 청국장이 더욱 높은 경우도 있었습니다. 거기다 보통은 찌게 형태로 조리해서 먹기 왜냐하면 더 이소플라본 섭취 비율은 줄어들 수밖에 없습니다. 물과 다른 재료가 들어가서 희석되는 데다가 이치 과정에서 열을 가하면서 다수의 이소플라본이 파괴된다고 합니다. 같은 100g을 먹어도 된장찌개나 청국장찌개에 들어있는 이소플라본은 더더욱 적다는 당연한 이야기입니다.
 두부나 두유 같은 컨디션 콩을 손찌검 과정에서 이소플라본이 다량 들어있는 부위(배아)를 떼어낸다고 합니다. 그래서 원재료보다 이소플라본 함량이 무진 적다고 합니다. 두부는 100g당 10mg 이하, 두유는 100g당 12mg 정도라고 하니 담화 다했지요. 사기당하는 느낌이 야외 정도입니다. 떼어낸 배아는 버리는 것이 아니라 약이나 보조식품 제조용으로 들어간다고 하니 진성 사기당하는 느낌이지요. 다른 사례로 품성 두부는 만들 도리 콩을 살짝 쓰는지 이소플라본 비율이 높게 나타났다고도 합니다.
 우리가 쉴손 먹게 되는 콩류는 익금 정도가 한계일 것 같습니다. 결과적으로 대두를 제출물로 먹는 경우가 아니면 이소플라본 섭취량으로 걱정할 이유는 없다는 말이 됩니다.


## 결론
 콩 속의 이소플라본은 건강에 좋은 영양분이긴 오히려 과도하게 섭취할 사연 부작용이 나타날 목숨 있습니다. 반면 과도한 수준은 진시 과도한 수준으로 볼만한 양이기 왜냐하면 딱히 걱정할 수준은 아니라고 볼 복수 있습니다. 모든 영양분 이야기가 나오면 같은 결론이 나오는데, 뭐든 과도하거나 부족하면 남김없이 문제를 일으킬 뿐이지요.
 아래는 길미 글과 관련이 있거나 참조한 문헌들입니다:

##### '건강상식' 카테고리의 다른 글

### 태그

### 관련글

### 댓글0
